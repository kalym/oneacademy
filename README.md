# Installation wizard for October

The wizard installation is a recommended way to install October. It is simpler than the command-line installation and doesn't require any special skills.

1. Prepare a directory on your server that is empty. It can be a sub-directory, domain root or a sub-domain.
1. [Download the installer archive file](https://github.com/octobercms/install/archive/master.zip).
1. Unpack the installer archive to the prepared directory.
1. Grant writing permissions on the installation directory and all its subdirectories and files.
1. Navigate to the install.php script in your web browser.
1. Follow the installation instructions.

## Minimum System Requirements

October CMS has a few system requirements:

* PHP 5.5.9 or higher
* PDO PHP Extension
* cURL PHP Extension
* MCrypt PHP Extension
* ZipArchive PHP Library
* GD PHP Library

As of PHP 5.5, some OS distributions may require you to manually install the PHP JSON extension.
When using Ubuntu, this can be done via ``apt-get install php5-json``.

# Migrations

1. run migrations: php artisan october:up
E:\workplace\MAMP\bin\php\php7.0.9\php artisan october:up

How to guide
For uploading to Red-Carlos Server etc.
1. connect by sftp to the server, download from there database config file (red-carlos path: /srv/www/academy/config/database.php) or keep it in mind.
2. connect to server through ssh
3. Type this commands step by step:
 # cd /srv/www/academy
 # git stash
 # git pull
 if there was a problem, investigate what files cause a problem and remove them from server.
 Next change file permissions:
 # chown -R www-data:www-data *

 upload database config file back to the server(or make it same as it was on beginning) and run migrations
 # php artisan october:up
4. Have fun in testing all stuff