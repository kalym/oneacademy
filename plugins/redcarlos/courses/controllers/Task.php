<?php namespace redcarlos\Courses\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Task extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'courses.manage' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('redcarlos.Courses', 'courses-menu', 'side-menu-tasks');
    }
}