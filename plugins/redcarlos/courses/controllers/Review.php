<?php namespace redcarlos\Courses\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Review extends Controller
{
    public $implement = ['Backend\Behaviors\FormController'];
    
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'courses.manage' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('redcarlos.Courses', 'courses-menu', 'side-menu-review');
    }
}