<?php namespace redcarlos\Courses\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Illuminate\Support\Facades\Redirect;
use October\Rain\Support\Facades\Flash;
use RedCarlos\Cabinet\Models\Dialog;

class Group extends Controller
{
    public $implement = ['Backend\Behaviors\ListController', 'Backend\Behaviors\FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('redcarlos.Courses', 'courses-menu', 'sub-menu-groups');
    }

    public function run($action = null, $params = [])
    {
        $this->addJs('/plugins/redcarlos/courses/assets/group.js');

        return parent::run($action, $params);
    }

    /**
     *
     */
    public function create_onSave()
    {
        $data = post('Group');

        $group = new \redcarlos\Courses\Models\Group;

        $this->saveGroup($group, $data);

        Flash::success('Группа создана');

        return Redirect::to("/backend/redcarlos/courses/group/update/".$group->id);
    }

    public function update_onSave()
    {
        $groupId = $this->params[0];

        $group = \redcarlos\Courses\Models\Group::find($groupId);

        $this->saveGroup($group, post('Group'));

        Flash::success('Группа обновлена');
        return Redirect::to("/backend/redcarlos/courses/group/update/".$group->id);
    }

    private function saveGroup($group, $data)
    {

        $group->title = $data['title'];
        $group->course_id = $data['course'];

        $group->save();


        if (key_exists('user_ids', $data)) {

            if ( $data['user_ids'] == 0 ) {
                $group->users()->detach();
            } else {
                $users = $group->users;

                $existUsers = [];

                foreach ( $users as $user) {
                    $existUsers[] = $user->id;
                }

                $group->users()->sync($data['user_ids']);

                $newUsers = [];

                foreach ( @$data['user_ids'] as $id => $value) {
                    if ( !in_array($value, $existUsers)) {
                        $newUsers[] = $value;
                    }
                }

                if ( count($newUsers) > 0 ) {
                    $this->generateDialogs($newUsers,$data['user_ids']);
                }
            }
        }
    }

    private function generateDialogs(array $newUsers, array $users)
    {
        $count = count($newUsers);
        for ( $i = 0;  $i < $count; $i++  ) {
            $current = $newUsers[$i];
            $users = array_diff($users,[$current]);

            foreach ( $users as $id )  {
                $dialog= new Dialog();
                $dialog->save();

                $dialog->users()->attach([$id , $current]);
            }
        }
    }
}