<?php namespace redcarlos\Courses\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use redcarlos\Courses\Models\Task;
use redcarlos\Courses\Models\Week;

class Lesson extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'courses.manage' 
    ];


    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('redcarlos.Courses', 'courses-menu', 'side-menu-lessons');
    }

    public function create_onSave()
    {
        $inputs = post('Lesson');


        $lesson = \redcarlos\Courses\Models\Lesson::create([
            'video' => $this->matchYoutubeVideoUrl($inputs['video']),
            'week_id' => $inputs['week_id'],
            'title' => $inputs['title'],
            ]);

        Task::create([
            'content' => $inputs['task_content'],
            'lesson_id' => $lesson->id
        ]);


        \Flash::success("Lesson created successfully");

        return $this->makeRedirect('update', $lesson);
    }


    public function update_onSave()
    {
        $inputs = post('Lesson');

        $lesson = \redcarlos\Courses\Models\Lesson::find($this->params[0]);
        $lesson->title = $inputs['title'];
        $lesson->video = $this->matchYoutubeVideoUrl($inputs['video']);
        $lesson->week_id = $inputs['week_id'];

        $lesson->save();

        $task = $lesson->task;
        $task->content = $inputs['task_content'];
        $task->save();


        \Flash::success("Lesson updated successfully");

        return $this->makeRedirect('update', $lesson);
    }

    private function matchYoutubeVideoUrl($url)
    {
        preg_match('/(?:https?:\/\/)?(?:www\.)?(?:m\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=|embed\/)?([0-9a-zA-Z_\-]+)(.+)?/', $url, $result);

        if(!isset($result[1]))
            $result[1]='';
        return '//www.youtube.com/embed/'.$result[1];
    }
}