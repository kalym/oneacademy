/**
 * Created by Maxim.Drobonoh on 19.01.2017.
 */
$( document ).ready(function() {


    $('#Form-field-Group-users_list').prop("selectedIndex", -1);

    if ( !$('.field-checkboxlist').length ) {
        $('#Form-field-Group-user_ids-group').append('<div class="field-checkboxlist"></div>');

        $('.field-checkboxlist').css('min-height', '150px');
    }

    $("#Form-field-Group-users_list").prepend("<option value='-1'>Выберите пользователя</option>").val();

    $("#Form-field-Group-user_ids-group :input").each(function() {
        $(this).prop('checked', true);
    });

    //todo add click on select




    $('#Form-field-Group-users_list').change(function (e) {
        e.preventDefault();

        var name = "";
        $( "#Form-field-Group-users_list option:selected" ).each(function() {
            name += $( this ).text() + " ";

        });

        var user = {
            'name': name,
            'id': $(this).val()
        };

        addUserToGroup(user);

        $("#Form-field-Group-users_list option[value='" + user.id + "']").remove();

    });

    function addUserToGroup(user) {


        var template = '<div class="checkbox custom-checkbox">' +
            '<input type="checkbox" checked id="checkbox_Form-field-Group-id_' + user.id + '" name="Group[user_ids][]" value="' + user.id + '">' +
            '<label for="checkbox_Form-field-Group-id_' + user.id + '">' + user.name + '</label></div>';

        $('.field-checkboxlist').append(template);

    }


});