<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRedcarlosCoursesTask3 extends Migration
{
    public function up()
    {
        Schema::table('redcarlos_courses_task', function($table)
        {
            $table->dropColumn('video');
        });
    }
    
    public function down()
    {
        Schema::table('redcarlos_courses_task', function($table)
        {
            $table->string('video', 255);
        });
    }
}
