<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRedcarlosCoursesWeekDialogs2 extends Migration
{
    public function up()
    {
        Schema::table('redcarlos_courses_week_dialogs', function($table)
        {
            $table->integer('student_id')->unsigned();
            $table->integer('instructor_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('redcarlos_courses_week_dialogs', function($table)
        {
            $table->dropColumn('student_id');
            $table->dropColumn('instructor_id');
        });
    }
}
