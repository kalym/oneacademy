<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRedcarlosCoursesWeekComments extends Migration
{
    public function up()
    {
        Schema::table('redcarlos_courses_week_comments', function($table)
        {
            $table->integer('wee_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('redcarlos_courses_week_comments', function($table)
        {
            $table->dropColumn('wee_id');
        });
    }
}
