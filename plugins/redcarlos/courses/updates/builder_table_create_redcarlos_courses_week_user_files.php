<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRedcarlosCoursesWeekUserFiles extends Migration
{
    public function up()
    {
        Schema::create('redcarlos_courses_week_user_files', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id');
            $table->integer('week_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('redcarlos_courses_week_user_files');
    }
}
