<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteRedcarlosCoursesTaskReviews extends Migration
{
    public function up()
    {
        Schema::dropIfExists('redcarlos_courses_task_reviews');
    }
    
    public function down()
    {
        Schema::create('redcarlos_courses_task_reviews', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('task_id');
            $table->integer('user_id');
        });
    }
}
