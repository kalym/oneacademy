<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRedcarlosCoursesList2 extends Migration
{
    public function up()
    {
        Schema::table('redcarlos_courses_list', function($table)
        {
            $table->dateTime('started_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('redcarlos_courses_list', function($table)
        {
            $table->dropColumn('started_at');
        });
    }
}
