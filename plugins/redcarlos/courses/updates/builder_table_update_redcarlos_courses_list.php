<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRedcarlosCoursesList extends Migration
{
    public function up()
    {
        Schema::table('redcarlos_courses_list', function($table)
        {
            $table->increments('id')->nullable(false)->unsigned()->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('redcarlos_courses_list', function($table)
        {
            $table->bigIncrements('id')->nullable(false)->unsigned()->default(null)->change();
        });
    }
}
