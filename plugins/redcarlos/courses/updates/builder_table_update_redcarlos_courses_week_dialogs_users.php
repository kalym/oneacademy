<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRedcarlosCoursesWeekDialogsUsers extends Migration
{
    public function up()
    {
        Schema::rename('redcarlos_courses_week_comments_users', 'redcarlos_courses_week_dialogs_users');
        Schema::table('redcarlos_courses_week_dialogs_users', function($table)
        {
            $table->renameColumn('comment_id', 'dialog_id');
        });
    }
    
    public function down()
    {
        Schema::rename('redcarlos_courses_week_dialogs_users', 'redcarlos_courses_week_comments_users');
        Schema::table('redcarlos_courses_week_comments_users', function($table)
        {
            $table->renameColumn('dialog_id', 'comment_id');
        });
    }
}
