<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRedcarlosCoursesTask extends Migration
{
    public function up()
    {
        Schema::create('redcarlos_courses_task', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('lesson_id');
            $table->text('content');
            $table->text('content_html');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('redcarlos_courses_task');
    }
}
