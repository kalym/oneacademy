<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRedcarlosCoursesQNA extends Migration
{
    public function up()
    {
        Schema::create('redcarlos_courses_qna', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('week_id');
            $table->integer('user_id');
            $table->string('video');
        });
    }

    public function down()
    {
        Schema::dropIfExists('redcarlos_courses_qna');
    }
}
