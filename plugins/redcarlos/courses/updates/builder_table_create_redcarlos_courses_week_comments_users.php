<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRedcarlosCoursesWeekCommentsUsers extends Migration
{
    public function up()
    {
        Schema::create('redcarlos_courses_week_comments_users', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('comment_id');
            $table->integer('user_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('redcarlos_courses_week_comments_users');
    }
}
