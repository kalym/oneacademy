<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRedcarlosCoursesWeeks2 extends Migration
{
    public function up()
    {
        Schema::table('redcarlos_courses_weeks', function($table)
        {
            $table->boolean('current')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('redcarlos_courses_weeks', function($table)
        {
            $table->dropColumn('current');
        });
    }
}
