<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRedcarlosCoursesReviews extends Migration
{
    public function up()
    {
        Schema::table('redcarlos_courses_reviews', function($table)
        {
            $table->integer('user_id');
            $table->integer('week_id');
            $table->dropColumn('content');
        });
    }
    
    public function down()
    {
        Schema::table('redcarlos_courses_reviews', function($table)
        {
            $table->dropColumn('user_id');
            $table->dropColumn('week_id');
            $table->text('content');
        });
    }
}
