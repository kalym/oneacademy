<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRedcarlosCoursesWeekDialogs extends Migration
{
    public function up()
    {
        Schema::rename('redcarlos_courses_week_comments', 'redcarlos_courses_week_dialogs');
    }
    
    public function down()
    {
        Schema::rename('redcarlos_courses_week_dialogs', 'redcarlos_courses_week_comments');
    }
}
