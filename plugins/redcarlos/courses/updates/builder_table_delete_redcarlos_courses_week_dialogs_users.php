<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteRedcarlosCoursesWeekDialogsUsers extends Migration
{
    public function up()
    {
        Schema::dropIfExists('redcarlos_courses_week_dialogs_users');
    }
    
    public function down()
    {
        Schema::create('redcarlos_courses_week_dialogs_users', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('dialog_id');
            $table->integer('user_id');
        });
    }
}
