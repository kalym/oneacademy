<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRedcarlosCoursesGroups2 extends Migration
{
    public function up()
    {
        Schema::table('redcarlos_courses_groups', function($table)
        {
            $table->string('title', 255);
        });
    }
    
    public function down()
    {
        Schema::table('redcarlos_courses_groups', function($table)
        {
            $table->dropColumn('title');
        });
    }
}
