<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRedcarlosCoursesGroups extends Migration
{
    public function up()
    {
        Schema::create('redcarlos_courses_groups', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('course_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('redcarlos_courses_groups');
    }
}
