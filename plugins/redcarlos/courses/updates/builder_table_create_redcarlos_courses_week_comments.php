<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRedcarlosCoursesWeekComments extends Migration
{
    public function up()
    {
        Schema::create('redcarlos_courses_week_comments', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('redcarlos_courses_week_comments');
    }
}
