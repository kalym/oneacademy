<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRedcarlosCoursesTask2 extends Migration
{
    public function up()
    {
        Schema::table('redcarlos_courses_task', function($table)
        {
            $table->dropColumn('content_html');
        });
    }
    
    public function down()
    {
        Schema::table('redcarlos_courses_task', function($table)
        {
            $table->text('content_html');
        });
    }
}
