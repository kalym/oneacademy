<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRedcarlosCoursesDialogWeeks extends Migration
{
    public function up()
    {
        Schema::create('redcarlos_courses_dialog_weeks', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('week_id');
            $table->integer('dialog_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('redcarlos_courses_dialog_weeks');
    }
}
