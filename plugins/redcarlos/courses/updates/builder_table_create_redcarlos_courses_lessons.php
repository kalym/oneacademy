<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRedcarlosCoursesLessons extends Migration
{
    public function up()
    {
        Schema::create('redcarlos_courses_lessons', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('week_id');
            $table->string('title', 255);
            $table->text('description');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('redcarlos_courses_lessons');
    }
}
