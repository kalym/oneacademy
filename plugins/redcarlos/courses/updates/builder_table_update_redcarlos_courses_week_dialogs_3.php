<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRedcarlosCoursesWeekDialogs3 extends Migration
{
    public function up()
    {
        Schema::table('redcarlos_courses_week_dialogs', function($table)
        {
            $table->renameColumn('wee_id', 'week_id');
        });
    }
    
    public function down()
    {
        Schema::table('redcarlos_courses_week_dialogs', function($table)
        {
            $table->renameColumn('week_id', 'wee_id');
        });
    }
}
