<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRedcarlosCoursesGroupsUsers extends Migration
{
    public function up()
    {
        Schema::create('redcarlos_courses_groups_users', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('group_id');
            $table->integer('user_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('redcarlos_courses_groups_users');
    }
}
