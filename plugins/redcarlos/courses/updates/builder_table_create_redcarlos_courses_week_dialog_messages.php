<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRedcarlosCoursesWeekDialogMessages extends Migration
{
    public function up()
    {
        Schema::create('redcarlos_courses_week_dialog_messages', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('content');
            $table->integer('dialog_id')->unsigned();
            $table->integer('user_id');
            $table->dateTime('sent_at');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('redcarlos_courses_week_dialog_messages');
    }
}
