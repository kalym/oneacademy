<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRedcarlosCoursesLessons extends Migration
{
    public function up()
    {
        Schema::table('redcarlos_courses_lessons', function($table)
        {
            $table->string('video');
            $table->dropColumn('description');
        });
    }
    
    public function down()
    {
        Schema::table('redcarlos_courses_lessons', function($table)
        {
            $table->dropColumn('video');
            $table->text('description');
        });
    }
}
