<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRedcarlosCoursesReviews extends Migration
{
    public function up()
    {
        Schema::create('redcarlos_courses_reviews', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('video');
            $table->text('content');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('redcarlos_courses_reviews');
    }
}
