<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRedcarlosCoursesTask extends Migration
{
    public function up()
    {
        Schema::table('redcarlos_courses_task', function($table)
        {
            $table->string('video');
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('redcarlos_courses_task', function($table)
        {
            $table->dropColumn('video');
            $table->increments('id')->unsigned()->change();
        });
    }
}
