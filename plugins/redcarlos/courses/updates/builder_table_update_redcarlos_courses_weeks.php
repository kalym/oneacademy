<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRedcarlosCoursesWeeks extends Migration
{
    public function up()
    {
        Schema::table('redcarlos_courses_weeks', function($table)
        {
            $table->integer('course_id');
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('redcarlos_courses_weeks', function($table)
        {
            $table->dropColumn('course_id');
            $table->increments('id')->unsigned()->change();
        });
    }
}
