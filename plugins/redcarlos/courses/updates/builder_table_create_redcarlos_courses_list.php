<?php namespace redcarlos\Courses\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRedcarlosCoursesList extends Migration
{
    public function up()
    {
        Schema::create('redcarlos_courses_list', function($table)
        {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->string('title', 255);
            $table->text('description');
            $table->string('slug', 255);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('redcarlos_courses_list');
    }
}
