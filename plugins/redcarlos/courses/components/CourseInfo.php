<?php
/**
 * Created by PhpStorm.
 * User: Maxim.Drobonoh
 * Date: 26.01.2017
 * Time: 17:31
 */

namespace redcarlos\courses\components;


use Cms\Classes\ComponentBase;
use redcarlos\Courses\Models\Course;

class CourseInfo extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'course-info',
            'description' => 'course-info'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title' => 'slug',
                'description' => 'slug',
                'default' => '{{ :slug }}',
                'type' => 'string'
            ],
        ];
    }


    function init()
    {
        $course = $this->loadCourse();

        $this->page['course'] = $course;
    }

    public function loadCourse()
    {
        $slug = $this->property('slug');

        $course = Course::where('slug', $slug)->first();

        return $course;
    }
}