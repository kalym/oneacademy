<?php
/**
 * Created by PhpStorm.
 * User: Maxim.Drobonoh
 * Date: 19.01.2017
 * Time: 19:09
 */

namespace redcarlos\courses\components;


use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use RainLab\User\Facades\Auth;
use RedCarlos\Cabinet\Models\Dialog;
use RedCarlos\Cabinet\Models\Message;
use redcarlos\Courses\Models\Course;
use redcarlos\Courses\Models\Week as WeekModel;
use redcarlos\Courses\Models\WeekDialog;
use redcarlos\Courses\Models\WeekDialogMessage;

class Week extends ComponentBase
{
    const FIRST_WEEK = 0;

    public function componentDetails()
    {
        return [
            'name' => 'week',
            'description' => 'week'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title' => 'slug',
                'description' => 'slug',
                'default' => '{{ :slug }}',
                'type' => 'string'
            ],
        ];
    }

    public function onRenderComments()
    {
        return $this->renderPartial('@comments');
    }

    public function onRun()
    {
        if ( !Auth::getUser()) {
            return Redirect::to('/course-info/' . $this->property('slug'));
        }
        parent::onRun();

        $this->addCss('assets/css/comments-loader.css');
        $this->addJs('assets/ajax-comments.js');
    }

    function init()
    {
        $user = Auth::getUser();

        if ( $user) {

            $course = $this->getCourseBySlug();
            $weeks = $course->getWeeks();

            if (isset($_REQUEST['week_id'])) {
                $week = $this->getRequestedWeek();
            } else {
              //TODO: need refactoring
                foreach ($weeks as $w){
                    if($w->current)
                        $week = $w;
                }
            }

            if (isset($_REQUEST['pageNumber'])) {
                $this->setCurrentPaginatorPage();
            }

            //TODO: remove mock user id
            $instructorId = 1;

            $weekDialog = $this->getDialog($user, $instructorId, $week);
            $messages = $weekDialog->messages()->paginate(5);

            $review = $week->review;
            $qna = $week->qna;

            if ($review)
                $this->page['review_video_url'] = $this->matchYoutubeVideoUrl($review->video);

            if ($qna)
                $this->page['qna_video_url'] = $this->matchYoutubeVideoUrl($qna->video);

            $userWeekFiles = $user->weekFiles($week->id);
            $this->registerFileUploader($userWeekFiles);

            if(!$week->current)
                $this->page['fileList'] = $user->weekFiles($_REQUEST['week_id'])->files;

            $this->page['weeks'] = $weeks;
            $this->page['week_next'] = $course->weeks->count() + 1;
            $this->page['week'] = $week;
            $this->page['user'] = $user;
            $this->page['dialog'] = $weekDialog;
            $this->page['dialog_id'] = $weekDialog->id;
            $this->page['messages'] = $messages;



        }
    }

    private function registerFileUploader($userWeekFiles)
    {
        $fileUploader = $this->addComponent(
            'Responsiv\Uploader\Components\FileUploader',
            'fileUploader',
            ['deferredBinding' => false]
        );
        $fileUploader->isMulti = true;
        $fileUploader->bindModel('files', $userWeekFiles);

        if (Request::ajax()) {
            $fileUploader->fileList = $fileUploader->getFileList();
            $fileUploader->singleFile = $fileUploader->fileList->first();
        }
    }

    private function setCurrentPaginatorPage()
    {
        $currentPage = post('pageNumber');

        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
    }

    private function getDialog($student, $instructorId, $week)
    {
        $weekDialog = $student->studentWeekDialog->where('week_id',$week->id)->where('instructor_id',$instructorId)->first();

        if (is_null($weekDialog)) {
            $weekDialog = new WeekDialog();
            $weekDialog->week_id = $week->id;
            $weekDialog->student_id = $student->id;
            $weekDialog->instructor_id = $instructorId;

            $weekDialog->save();
        }
        return $weekDialog;
    }

    public function onSendComment()
    {
        $dialogId = post('dialog_id');
        $content = post('content');

        $weekDialog = WeekDialog::find($dialogId);

        WeekDialogMessage::createMessage([
            'dialog_id' => $dialogId,
            'content' => $content,
            'user_id' => Auth::getUser()->id,
            'sent_at' => Carbon::now()
        ]);
        $this->page['messages'] = $weekDialog->messages;

        return $this->renderPartial('@comments');
    }


    public function onLoadWeek()
    {
        $this->page['week'] = $this->getRequestedWeek();

        return [
            'content' => $this->renderPartial('@week'),
            'title' => $this->renderPartial('@week_title')
        ];
    }

    protected function getRequestedWeek()
    {
        $weekId = $_REQUEST['week_id'];
        return WeekModel::where('id', $weekId)->with('lesson')->first();
    }

    public function getCourseBySlug()
    {
        $slug = $this->property('slug');
        $course = Course::where('slug', $slug)->first();

        return $course;
    }

    private function matchYoutubeVideoUrl($url)
    {
        preg_match('/(?:https?:\/\/)?(?:www\.)?(?:m\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=|embed\/)?([0-9a-zA-Z_\-]+)(.+)?/', $url, $result);

        return '//www.youtube.com/embed/' . $result[1];
    }
}