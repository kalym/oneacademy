<?php
/**
 * Created by PhpStorm.
 * User: Maxim.Drobonoh
 * Date: 16.01.2017
 * Time: 18:31
 */

namespace  redcarlos\courses\components;


use Cms\Classes\ComponentBase;
use RainLab\User\Facades\Auth;
use redcarlos\Courses\Models\Course;

class Courses extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Courses list',
            'description' => 'Courses list'
        ];
    }

    public function onRun()
    {
        parent::onRun();

        $courses = Course::all();

        foreach ( $courses as $course ) {
            $course->page_url = Auth::getUser() ? "/course/".$course->slug  : "/course-info/".$course->slug;
        }

        $this->page['courses'] =  $courses;
    }
}