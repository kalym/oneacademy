<?php namespace redcarlos\Courses\Models;

use Carbon\Carbon;
use Model;

/**
 * Model
 */
class Course extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'redcarlos_courses_list';

    public $attachOne = [
        'course_image' => 'System\Models\File'
    ];

    public $hasMany = [
        'weeks' => ['\RedCarlos\Courses\Models\Week']
    ];

    public function getWeeks()
    {
        $weeks =  $this->weeks()->with(['lesson' => function ($query) {
            $query->with('task');
        }])->get();
// TODO: Зло! истребить!
        foreach ($weeks as $week){
            $week->current = 0;
            $week->save();
        }

        $weekNow = $this->getCurrentWeek();

        $weeksLock = $weeks->count() - $weekNow;
        //TODO: need refactoring

        if(isset($weeks[$weekNow])) {
            $weeks[$weekNow]->current = 1;
            $weeks[$weekNow]->save();
        }

        if ( $weeksLock > 0 ) {
            for ($i = $weekNow+1; $i < $weeks->count(); $i++) {
                if(isset($weeks[$i]))
                    $weeks[$i]->lock = true;
            }
        }
        return $weeks;
    }

    public function getCurrentWeek(){

        $startedAt = Carbon::parse($this->started_at);

        return $startedAt->diffInWeeks(Carbon::now());
    }
}