<?php namespace redcarlos\Courses\Models;

use Model;

/**
 * Model
 */
class WeekDialog extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'redcarlos_courses_week_dialogs';

    public $belongsToMany = [
        'student' => [
            '\RainLab\User\Models\User',
            'key' => 'student_id'
        ],
        'instructor' => [
            '\RainLab\User\Models\User',
            'key' => 'instructor_id'
        ]
    ];

    public $hasMany = [
        'messages' => ['\RedCarlos\Courses\Models\WeekDialogMessage', 'key' => 'dialog_id']
    ];
}