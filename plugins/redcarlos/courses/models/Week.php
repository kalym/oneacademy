<?php namespace redcarlos\Courses\Models;

use Model;

/**
 * Model
 */
class Week extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /*
    * Relations
    */
    public $belongsTo = [
        'course' => ['\RedCarlos\Courses\Models\Course']
    ];

    public $hasOne = [
        'lesson' => ['\RedCarlos\Courses\Models\Lesson'],
        'review' => ['\RedCarlos\Courses\Models\Review'],
        'qna' => ['\RedCarlos\Courses\Models\QNA']
    ];

    public $attachMany = [
        'files' => 'System\Models\File',
    ];

    public $belongsToMany = [
        'dialogs' => [
            '\RedCarlos\Cabinet\Models\Dialog',
            'table' => 'redcarlos_courses_dialog_weeks',
            'key' => 'week_id',
            'otherKey' => 'dialog_id'
        ]];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'redcarlos_courses_weeks';


    public function getCourseIdOptions()
    {
        $items = [];

        $courses = Course::all();

        foreach ($courses as $course) {
            $items[$course->id] = $course->title;
        }

        return $items;
    }

    public function isLocked()
    {
        return isset($this->lock) ? true : false;
    }


}