<?php namespace redcarlos\Courses\Models;

use Carbon\Carbon;
use Model;

/**
 * Model
 */
class WeekDialogMessage extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'redcarlos_courses_week_dialog_messages';

    public static function createMessage($data)
    {
        $msg = new static;
        $msg->user_id = $data['user_id'];
        $msg->dialog_id = $data['dialog_id'];
        $msg->content = $data['content'];
        $msg->sent_at = Carbon::now();

        $msg->save();

        return $msg;
    }

    public function frontendDate()
    {
        Carbon::setLocale('ru');
        return Carbon::parse($this->sent_at)->diffForHumans();
    }
}