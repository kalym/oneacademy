<?php namespace redcarlos\Courses\Models;

use Model;

/**
 * Model
 */
class Lesson extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'redcarlos_courses_lessons';

    public $fillable = ['week_id', 'title', 'video'];

    public $guarded = ['course_id'];

    public $attachOne = [
        'posterImage' => 'System\Models\File'
    ];

    /*
    * Relations
    */
    public $belongsTo = [
        'week' => ['\RedCarlos\Courses\Models\Week'],
    ];

    public $hasOne = [
        'task' => ['\RedCarlos\Courses\Models\Task'],
    ];

    private $fistCourse  = null;

    public function getWeekIdOptions()
    {
        $items = [];

        if ( $this->fistCourse ) {
            $weeks  = $this->fistCourse->weeks;

            foreach ( $weeks as $week ) {
                $items[$week->id] = $week->name;
            }
        }
        return $items;
    }

    public function getCourseIdOptions()
    {
        $items = [];
        $courses = Course::all();

        if ( $this->hasItems($courses) ) {

            foreach ($courses as $course) {
                $items[$course->id] = $course->title;
            }

            $this->setFirstCourse($courses->get(0));
        }

        return $items;
    }

    private function setFirstCourse($course)
    {
        $this->fistCourse = $course;
    }

    public function filterFields($fields)
    {

        $courseId = $fields->course_id->value;

        $course = Course::find($courseId);

        if (!$course)
            return;

        $weeks = $course->weeks;

        if ($this->hasItems($weeks)) {
            $options = [];
            foreach ($weeks as $week) {
                $options[$week->id] = $week->name;
            }
            $fields->week_id->options = $options;
        }
    }

    public function getCourseAttribute()
    {
        if ( $this->week ) {
            $courseId = $this->week()->first()->course_id;

            return Course::find($courseId)->title;
        }
    }

    public function hasItems($collection)
    {
        return $collection->count() > 0;
    }

    public function getSelectedCourse($field)
    {
        $course = null;

        if ( $field->value ) {
            $course = Course::find($field->value) ;
        }
        return $course;
    }

}