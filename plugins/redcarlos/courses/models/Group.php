<?php namespace redcarlos\Courses\Models;

use Backend\Classes\FormField;
use Illuminate\Support\Facades\DB;
use Model;
use RainLab\User\Models\User;


/**
 * Model
 */
class Group extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
        * Relations
    */
    public $belongsTo = [
        'course' => ['\RedCarlos\Courses\Models\Course']
    ];

    public $belongsToMany = [
        'users' => [
            '\RainLab\User\Models\User',
            'table' => 'redcarlos_courses_groups_users',
            'key' => 'group_id',
            'otherKey' => 'user_id'
        ]];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'redcarlos_courses_groups';

    public function getCourseIdOptions()
    {

        $items = [];

        $courses = Course::all();

        foreach ($courses as $course) {
            $items[$course->id] = $course->title;
        }

        return $items;
    }


    public function getUserIdsOptions()
    {
        $groupUsers = $this->users;

        $usersList = [];

        foreach ($groupUsers as $user) {
            $usersList[$user->id] = $user->name;
        }

        return $usersList;
    }

    public function getUsersListOptions($field)
    {

        if ($field) {
            $users = User::where('name', 'like', '%' . $field . '%')->get();
        } else {

            $users = User::whereNotIn('id', function ($query) {
                $query
                    ->select('redcarlos_courses_groups_users.user_id')
                    ->from('redcarlos_courses_groups_users')
                    ->where('redcarlos_courses_groups_users.group_id', $this->id);
            })->get();
        }

        $options = [];

        foreach ($users as $user) {
            $options[$user->id] = $user->name;
        }

        return $options;
    }
}