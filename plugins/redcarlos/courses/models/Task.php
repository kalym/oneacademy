<?php namespace redcarlos\Courses\Models;

use Model;

/**
 * Model
 */
class Task extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'redcarlos_courses_task';


    /*
     * Relations
    */
    public $belongsTo = [
        'lesson' => ['\RedCarlos\Courses\Models\Lesson'],
    ];

    public $fillable = ['lesson_id', 'content'];


    public function getLessonIdOptions()
    {
        $items = [];

        $lessons = Lesson::all();

        foreach ($lessons as $lesson) {
            $items[$lesson->id] = $lesson->title;
        }

        return $items;
    }
}