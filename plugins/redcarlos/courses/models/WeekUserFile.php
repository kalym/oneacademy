<?php namespace redcarlos\Courses\Models;

use Model;

/**
 * Model
 */
class WeekUserFile extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    public $attachMany = [
        'files' => 'System\Models\File',
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'redcarlos_courses_week_user_files';
}