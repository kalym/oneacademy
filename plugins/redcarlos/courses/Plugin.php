<?php namespace redcarlos\Courses;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Redcarlos\Courses\Components\Week' => 'week',
            'Redcarlos\Courses\Components\Courses' => 'courses',
            'Redcarlos\Courses\Components\CourseInfo' => 'courseinfo'
        ];
    }

    public function registerSettings()
    {
    }


    public function pluginDetails()
    {
        return [
            'name' => 'Courses',
            'description' => '',
            'author' => 'RedCarlos',
            'icon' => 'oc-icon-male'
        ];
    }
}
