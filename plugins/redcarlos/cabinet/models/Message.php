<?php namespace RedCarlos\Cabinet\Models;

use Model;
use RainLab\User\Models\User;

/**
 * Model
 */
class Message extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
    }
    /**
     * @var string The database table used by the model.
     */
    public $table = 'redcarlos_cabinet_messages';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function createMessage($data)
    {
        $msg = new static;
        $msg->user_id = $data['user_id'];
        $msg->dialog_id = $data['dialog_id'];
        $msg->content = $data['content'];

        $msg->save();



        return $msg;
    }
}