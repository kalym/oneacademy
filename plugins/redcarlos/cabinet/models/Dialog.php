<?php
/**
 * Created by PhpStorm.
 * User: Maxim.Drobonoh
 * Date: 13.01.2017
 * Time: 12:29
 */

namespace RedCarlos\Cabinet\Models;


use Model;
use Auth;
use RainLab\User\Models\User;

class Dialog extends Model
{
    public $timestamps = false;

    public $table = 'redcarlos_cabinet_dialogs';


    public $belongsToMany = [
        'weeks' => [
            '\RedCarlos\Courses\Models\Week',
            'table' => 'redcarlos_courses_dialog_weeks',
            'key' => 'dialog_id',
            'otherKey' => 'week_id'
        ],
        'users' => [
            '\RainLab\User\Models\User',
            'table' => 'redcarlos_cabinet_dialog_users',
            'key' => 'dialog_id',
            'otherKey' => 'user_id'
        ]
    ];

    public static function findDialogByWeekAndUser($week, $user)
    {
        $dialog = Dialog::join('redcarlos_courses_dialog_weeks', function ($join) use ($week) {
            $join->on('redcarlos_cabinet_dialogs.id', '=', 'redcarlos_courses_dialog_weeks.dialog_id')
                ->where('redcarlos_courses_dialog_weeks.week_id', '=', $week->id);
        })->join('redcarlos_cabinet_dialog_users', function ($join) use ($user) {
            $join->on('redcarlos_cabinet_dialogs.id', '=', 'redcarlos_cabinet_dialog_users.dialog_id')
                ->where('redcarlos_cabinet_dialog_users.user_id', '=', $user->id);
        })->first();

        return $dialog;
    }

    public function paginateMessages($count = 3)
    {
        return $this->join('redcarlos_cabinet_messages', function ($join)  {
            $join->on('redcarlos_cabinet_dialogs.id', '=', 'redcarlos_cabinet_messages.dialog_id');
        })->paginate($count);
    }

    public function allMessages()
    {
        return $this->join('redcarlos_cabinet_messages', function ($join)  {
            $join->on('redcarlos_cabinet_dialogs.id', '=', 'redcarlos_cabinet_messages.dialog_id');
        })->get();
    }


    public function messages()
    {
        return $this->hasMany(Message::class, 'dialog_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'redcarlos_cabinet_dialog_users',
            'dialog_id', 'user_id');
    }

    public function latestMessage()
    {
        return $this->hasOne(Message::class, 'dialog_id')->with('user')->latest('created_at')->first();
    }

    public function dialogUser()
    {
        $user =  $this->users()->where('users.id', '!=', Auth::getUser()->id)->first();

        return $user;
    }

    public function hasNew()
    {
        return count($this->messages()->whereRaw(' is_new = 1 and user_id != ' . Auth::getUser()->id)->get()) > 0;
    }
}