<?php namespace RedCarlos\Cabinet\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRedcarlosCabinetDialogs extends Migration
{
    public function up()
    {
        Schema::create('redcarlos_cabinet_dialogs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->dateTime('last_active')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('redcarlos_cabinet_dialogs');
    }
}
