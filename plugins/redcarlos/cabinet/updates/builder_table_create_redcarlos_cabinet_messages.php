<?php namespace RedCarlos\Cabinet\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRedcarlosCabinetMessages extends Migration
{
    public function up()
    {
        Schema::create('redcarlos_cabinet_messages', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('dialog_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->text('content');
            $table->timestamp('created_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('redcarlos_cabinet_messages');
    }
}
