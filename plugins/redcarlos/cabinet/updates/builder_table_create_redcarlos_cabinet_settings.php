<?php namespace RedCarlos\Cabinet\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRedcarlosCabinetSettings extends Migration
{
    public function up()
    {
        Schema::create('redcarlos_cabinet_settings', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255);
            $table->text('value');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('redcarlos_cabinet_settings');
    }
}
