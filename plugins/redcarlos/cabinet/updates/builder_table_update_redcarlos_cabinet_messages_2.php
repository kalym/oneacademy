<?php namespace RedCarlos\Cabinet\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRedcarlosCabinetMessages2 extends Migration
{
    public function up()
    {
        Schema::table('redcarlos_cabinet_messages', function($table)
        {
            $table->boolean('is_new')->default(1);
        });
    }
    
    public function down()
    {
        Schema::table('redcarlos_cabinet_messages', function($table)
        {
            $table->dropColumn('is_new');
        });
    }
}
