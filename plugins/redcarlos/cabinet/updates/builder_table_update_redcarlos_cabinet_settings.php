<?php namespace RedCarlos\Cabinet\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRedcarlosCabinetSettings extends Migration
{
    public function up()
    {
        Schema::table('redcarlos_cabinet_settings', function($table)
        {
            $table->text('value')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('redcarlos_cabinet_settings', function($table)
        {
            $table->text('value')->nullable(false)->change();
        });
    }
}
