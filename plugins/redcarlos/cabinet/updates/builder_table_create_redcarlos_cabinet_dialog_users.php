<?php namespace RedCarlos\Cabinet\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateRedcarlosCabinetDialogUsers extends Migration
{
    public function up()
    {
        Schema::create('redcarlos_cabinet_dialog_users', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('dialog_id')->unsigned();
            $table->integer('user_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('redcarlos_cabinet_dialog_users');
    }
}
