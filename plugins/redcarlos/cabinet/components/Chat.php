<?php
/**
 * Created by PhpStorm.
 * User: Maxim.Drobonoh
 * Date: 13.01.2017
 * Time: 12:46
 */

namespace redcarlos\cabinet\components;

use Cms\Classes\Page;

use Cms\Classes\ComponentBase;
use Auth;
use RainLab\User\Models\User;
use RedCarlos\Cabinet\Models\Dialog;
use RedCarlos\Cabinet\Models\Message;

class Chat extends ComponentBase
{
    public $currentDialogId;

    private $months = array('','января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');

    public function componentDetails()
    {
        return [
            'name' => 'chat',
            'description' => 'offline chat'
        ];
    }

    public function onRun()
    {
        parent::onRun();

        $dialogs = $this->dialogs();

        if ( is_null($dialogs)) {
            return;
        }

        if (  $dialog = $dialogs->first() ) {

            $initMessages = $dialog->messages()->get();

            $this->prepareMessages($initMessages);

            $this->page['me_id'] = $this->user()->id;
            $this->page['dialogs'] = $dialogs;
            $this->page['messages'] = $initMessages;
        }
    }

    public function onTabOpen(){
        $dialogs = $this->dialogs();

        if ( $dialogs = $dialogs->first()) {
            $initMessages = $dialogs->first()->messages()->get();
            $this->markAsRead($initMessages);
        }
    }

    public function dialogs()
    {
        if (!$user = $this->user()) {
            return;
        }

        $dialogs = $user->dialogs();
        if(count($dialogs)<5) {
        //    $this->generateDialogs($this->user()->id);
          //  $dialogs = $this->dialogs();
        }

        return $dialogs;
    }

    public function getUsersFromDialogs($dialogs)
    {
        $users = [];

        foreach ($dialogs as $dialog) {
            $users[] = $dialog->dialogUser();
        }

        return $users;
    }

    public function getUserFromDialog($dialog)
    {

        $user = $dialog->users()->where('users.id','!=',$this->user()->id)->get();

        return $user;
    }

    public function onSendMessage()
    {
        $dialogId = post('chat-current-dialog-id');

        $dialog = Dialog::find($dialogId);
        $dialog->last_active = date('Y-m-d H:i:s');
        $dialog->save();

        $message = Message::createMessage([
            'user_id' => $this->user()->id,
            'content' => post('content'),
            'dialog_id' => $dialogId
        ]);

        $isSuccess = false;

        if ($message)
            $isSuccess = $this->formatDate($message->created_at);

        return $isSuccess;
    }

    public function onLoadMessages()
    {
        $dialogId = post('dialog_id');

        $dialog = Dialog::find($dialogId);

        $messages = $dialog->messages()->get();

        $this->markAsRead($messages);

        $this->prepareMessages($messages);

        $this->page['messages'] = $messages;

        return $this->renderPartial('@dialog');
    }

    public function prepareMessages($messages)
    {
        $meId = $this->user()->id;

        $messages->each(function ($msg) use ($meId) {

            if ($msg->user_id == $meId){
                $msg->isMe = true;
            }else{
                $msg->isMe = false;
            }

            $msg->created_at = $this->formatDate( $msg->created_at );

        });
    }

    public function markAsRead($messages){
        $messages->each(function ($msg) {
            $msg->is_new = 0;
            $msg->save();
        });
    }

    private function formatDate($date){
        $time = strtotime($date);
        $month = $this->months[(int)date('n',$time)];

        return date("d $month Y, H:i",$time);
    }


    /**
     * Returns the logged in user, if available
     */
    public function user()
    {
        if (!Auth::check()) {
            return null;
        }

        return Auth::getUser();
    }

    public function generateDialogs($id){
        $users = User::take(10)->where('id','!=',$id)->get();
        foreach ($users as $user){
            $dialog = new Dialog();
            $dialog->save();
            $dialog->users()->attach([$user->id,$id]);
        }
    }

    public function newMessagesCount(){
        $count = 0;
        $dialogs = $this->dialogs();
        foreach ($dialogs as $dialog){
            if($dialog->hasNew())
                $count++;
        }

        return $count;
    }
}