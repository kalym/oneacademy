<?php namespace RedCarlos\Cabinet\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Setting extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'cabinet.settings.editing' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('RedCarlos.Cabinet', 'cabinet-settings');
    }
}