<?php namespace RedCarlos\Cabinet;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Redcarlos\Cabinet\Components\Chat' => 'chat',
        ];
    }

    public function registerSettings()
    {
    }


    public function pluginDetails()
    {
        return [
            'name' => 'Cabinet',
            'description' => '',
            'author' => 'RedCarlos',
            'icon' => 'oc-icon-male'
        ];
    }
}
