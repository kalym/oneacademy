<?php namespace Flynsarmy\SocialLogin\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateFlynsarmySocialloginUserProviders extends Migration
{
    public function up()
    {
        Schema::table('flynsarmy_sociallogin_user_providers', function($table)
        {
            $table->string('avatar_url', 255)->nullable();
            $table->string('provider_id', 255)->default(null)->change();
            $table->string('provider_token', 255)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('flynsarmy_sociallogin_user_providers', function($table)
        {
            $table->dropColumn('avatar_url');
            $table->string('provider_id', 255)->default(null)->change();
            $table->string('provider_token', 255)->default(null)->change();
        });
    }
}
