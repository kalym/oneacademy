<?php namespace Flynsarmy\SocialLogin\SocialLoginProviders;


use Backend\Widgets\Form;
use Flynsarmy\SocialLogin\SocialLoginProviders\SocialLoginProviderBase;
use Flynsarmy\SocialLogin\Models\Settings;
use Hybrid_Endpoint;
use Hybrid_Auth;
use URL;

class Vk extends SocialLoginProviderBase
{
    use \October\Rain\Support\Traits\Singleton;

    /**
     * Initialize the singleton free from constructor parameters.
     */
    protected function init()
    {
        parent::init();
    }

    public function isEnabled()
    {
      return true;
    }

    public function extendSettingsForm(Form $form)
    {
        $form->addFields([
            'noop' => [
                'type' => 'partial',
                'path' => '$/flynsarmy/sociallogin/partials/backend/forms/settings/_vk_info.htm',
                'tab' => 'Vkontakte',
            ],

            'providers[Vkontakte][enabled]' => [
                'label' => 'Enabled?',
                'type' => 'checkbox',
                'default' => 'true',
                'tab' => 'Vkontakte',
            ],

            'providers[Vkontakte][api_key]' => [
                'label' => 'API Key',
                'type' => 'text',
                'tab' => 'Vkontakte',
            ],

            'providers[Vkontakte][api_secret]' => [
                'label' => 'API Secret',
                'type' => 'text',
                'tab' => 'Vkontakte',
            ],
        ], 'primary');
    }


    public function login($provider_name, $action)
    {
        // check URL segment
        if ($action == "auth") {
            Hybrid_Endpoint::process();

            return;
        }

        $providers = $this->settings->get('providers', []);

        // create a HybridAuth object
        $socialAuth = new Hybrid_Auth([
            "base_url" => URL::route('flynsarmy_sociallogin_provider', [$provider_name, 'auth']),
            "providers" => [
                'Vkontakte' => [
                    "enabled" => true,
                    "keys" => array("id" => @$providers['Vkontakte']['api_key'], "secret" => @$providers['Vkontakte']['api_secret']),
                    "scope" => "email, user_about_me",
                    "wrapper" => array(
                        "class" =>'Hybrid_Providers_Vkontakte',
                        "path" => $_SERVER['DOCUMENT_ROOT'].'/plugins/flynsarmy/sociallogin/vendor/hybridauth/hybridauth/additional-providers/hybridauth-vkontakte/Providers/Vkontakte.php'
                    )
                ]
            ],
        ]);

        // authenticate with Facebook
        $provider = $socialAuth->authenticate($provider_name);

        // fetch user profile
        $userProfile = $provider->getUserProfile();


        return [
            'token' => $userProfile->identifier,
            'email' => $userProfile->email,
            'username' => $userProfile->displayName,
            'name' => $userProfile->firstName . ' ' . $userProfile->lastName,
            'avatar_url' => $userProfile->photoURL
        ];
    }
}