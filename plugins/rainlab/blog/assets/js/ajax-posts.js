var page = 1;
var isAllLoaded = false;

window.onscroll = function(ev) {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        // you're at the bottom of the page
        page++;
        loadMoreData(page);
    }
};

function loadMoreData(page) {

        if ( !isAllLoaded) {
            $.request('onPostAsync', {
                data: {
                    pageNumber: page
                },
                beforeSend: function () {
                    $('.feed-ajax-loader').css('display', 'block');
                },
                success: function (response) {
                    console.log('Finished!');

                    $('.feed-ajax-loader').css('display', 'none');


                    if (response.length == 0 ) {
                        isAllLoaded = true;
                    }

                    $('.feed-list').append(response.result);
                }
            });
        }

    /*$.ajax({
        url: '/oneacademy/feed-item?page=' + page,
        type: "get",
        beforeSend: function () {

        },
        error: function () {

        }})
        .done(function (data) {
            console.log('post page: ' + page);
        });
     */
}