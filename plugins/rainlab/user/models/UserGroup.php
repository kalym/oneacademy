<?php namespace RainLab\User\Models;

use October\Rain\Auth\Models\Group as GroupBase;
use ApplicationException;

/**
 * User Group Model
 */
class UserGroup extends GroupBase
{
    const GROUP_GUEST = 'guest';
    const GROUP_REGISTERED = 'registered';
    const GROUP_INSTRUCTOR = 'instructor';

    /**
     * @var string The database table used by the model.
     */
    protected $table = 'user_groups';

    /**
     * Validation rules
     */
    public $rules = [
        'name' => 'required|between:3,64',
        'code' => 'required|regex:/^[a-zA-Z0-9_\-]+$/|unique:user_groups',
    ];

    /**
     * @var array Relations
     */
    public $belongsToMany = [
        'users'       => ['RainLab\User\Models\User', 'table' => 'users_groups'],
        'users_count' => ['RainLab\User\Models\User', 'table' => 'users_groups', 'count' => true]
    ];

    /**
     * @var array The attributes that are mass assignable.
     */
    protected $fillable = [
        'name',
        'code',
        'description'
    ];

    protected static $guestGroup = null;

    protected static $registerGroup = null;

    protected static $instructorGroup = null;

    /**
     * Returns the guest user group.
     * @return RainLab\User\Models\UserGroup
     */
    public static function getGuestGroup()
    {
        if (self::$guestGroup !== null) {
            return self::$guestGroup;
        }

        $group = self::where('code', self::GROUP_GUEST)->first() ?: false;

        return self::$guestGroup = $group;
    }

    /**
     * @return RainLab\User\Models\UserGroup
     */
    public static function getRegisteredGroup()
    {
        if (self::$registerGroup !== null) {
            return self::$registerGroup;
        }

        $group = self::where('code', self::GROUP_REGISTERED)->first() ?: false;

        return self::$registerGroup = $group;
    }

    public static function getInstructorGroup()
    {
        if (self::$instructorGroup !== null) {
            return self::$instructorGroup;
        }

        $group = self::where('code', self::GROUP_INSTRUCTOR)->first() ?: false;

        return self::$instructorGroup = $group;
    }
}
