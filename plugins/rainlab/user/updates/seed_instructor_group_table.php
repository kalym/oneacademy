<?php namespace RainLab\User\Updates;

use RainLab\User\Models\UserGroup;
use October\Rain\Database\Updates\Seeder;

class SeedInstructorGroupTable extends Seeder
{
    public function run()
    {
        UserGroup::create([
            'name' => 'Instructor',
            'code' => 'instructor',
            'description' => 'Default group for instructor users.'
        ]);
    }
}
