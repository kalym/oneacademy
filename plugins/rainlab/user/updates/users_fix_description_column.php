<?php namespace RainLab\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UsersFIxDescriptionColumn extends Migration
{

    public function up()
    {
        Schema::table('users', function ($table) {
            $table->text('description')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('description');
        });
    }
}