<?php namespace RainLab\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UsersAddDescriptionColumn extends Migration
{
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->text('description')->nullbable();
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->dropColumn('description');
        });
    }
}
