<?php namespace RainLab\Forum\Updates;

use RainLab\Forum\Models\Topic;
use Schema;
use Illuminate\Support\Facades\DB;
use October\Rain\Database\Updates\Seeder;
/**
 * Created by PhpStorm.
 * User: Maxim.Drobonoh
 * Date: 12.01.2017
 * Time: 13:15
 */
class SeedTopicsAndComments extends Seeder
{
    public function run()
    {
        Topic::create([
            'id' => 1,
            'subject' => 'Вебинар на тему «3D моделирование в Украине и мире»',
            'slug' => 'vebinar-na-temu-3d-modelirovanie-v-ukraine-i-mire',
            'channel_id' => 11,
            'start_member_id' => 0,
            'last_post_id' => 14,
            'last_post_member_id' => 1,
            'last_post_at' => '2017-01-04 09:02:42',
            'is_private' => 0,
            'is_sticky' => 0,
            'is_locked' => 0,
            'count_posts' => 13,
            'count_views' => 100,
            'created_at' => '2016-12-29 20:23:20',
            'updated_at' => '2017-01-04 06:02:42',
            'embed_code' => 'vebinar-na-temu-3d-modelirovanie-v-ukraine-i-mire'
        ]);


        Topic::create([
            'id' => 2,
            'subject' => 'Вебинар на тему «3D моделирование в Украине и мире»',
            'slug' => 'vebinar-na-temu-3d-modelirovanie-v-ukraine-i-mire',
            'channel_id' => 11,
            'start_member_id' => 0,
            'last_post_id' => 15,
            'last_post_member_id' => 1,
            'last_post_at' => '2017-01-04 09:02:42',
            'is_private' => 0,
            'is_sticky' => 0,
            'is_locked' => 0,
            'count_posts' => 0,
            'count_views' => 25,
            'created_at' => '2016-12-29 20:23:20',
            'updated_at' => '2017-01-04 06:02:42',
            'embed_code' => 'vebinar-3d'
        ]);



        Topic::create([
            'id' => 3,
            'subject' => 'First blog post',
            'slug' => 'first-blog-post',
            'channel_id' => 11,
            'start_member_id' => 0,
            'last_post_id' => 8,
            'last_post_member_id' => 1,
            'last_post_at' => '2017-01-04 09:02:42',
            'is_private' => 0,
            'is_sticky' => 0,
            'is_locked' => 0,
            'count_posts' => 1,
            'count_views' => 100,
            'created_at' => '2016-12-29 20:23:20',
            'updated_at' => '2017-01-04 06:02:42',
            'embed_code' => 'first-blog-post'
        ]);


        DB::statement("
            INSERT INTO `rainlab_forum_posts` (`id`, `subject`, `content`, `content_html`, `topic_id`, `member_id`, `edit_user_id`, `delete_user_id`, `created_at`, `updated_at`) VALUES
(1, 'Вебинар на тему «3D моделирование в Украине и мире»', 'Hello!', '<p>Hello!</p>', 1, 4, NULL, NULL, '2016-12-29 20:23:28', '2016-12-29 20:23:28'),
(2, 'Вебинар на тему «3D моделирование в Украине и мире»', 'hello', '<p>hello</p>', 1, 1, NULL, NULL, '2016-12-30 06:32:09', '2016-12-30 06:32:09'),
(3, 'Вебинар на тему «3D моделирование в Украине и мире»', 'ho', '<p>ho</p>', 1, 1, NULL, NULL, '2016-12-30 06:40:48', '2016-12-30 06:40:48'),
(4, 'Вебинар на тему «3D моделирование в Украине и мире»', 'asgsag', '<p>asgsag</p>', 1, 1, NULL, NULL, '2016-12-30 06:42:44', '2016-12-30 06:42:44'),
(5, 'Вебинар на тему «3D моделирование в Украине и мире»', 'sdsd', '<p>sdsd</p>', 1, 1, NULL, NULL, '2016-12-30 06:45:28', '2016-12-30 06:45:28'),
(6, 'Вебинар на тему «3D моделирование в Украине и мире»', 'test', '<p>test</p>', 1, 1, NULL, NULL, '2016-12-30 06:45:44', '2016-12-30 06:45:44'),
(7, 'Вебинар на тему «3D моделирование в Украине и мире»', 'yoyoy', '<p>yoyoy</p>', 1, 1, NULL, NULL, '2016-12-30 06:46:12', '2016-12-30 06:46:12'),
(8, 'First blog post', 'Djdj', '<p>Djdj</p>', 3, 1, NULL, NULL, '2016-12-30 08:13:19', '2016-12-30 08:13:19'),
(9, 'Вебинар на тему «3D моделирование в Украине и мире»', 'Test', '<p>Test</p>', 1, 1, NULL, NULL, '2017-01-04 05:56:22', '2017-01-04 05:56:22'),
(10, 'Вебинар на тему «3D моделирование в Украине и мире»', 'Yoyo', '<p>Yoyo</p>', 1, 1, NULL, NULL, '2017-01-04 05:56:37', '2017-01-04 05:56:37'),
(11, 'Вебинар на тему «3D моделирование в Украине и мире»', 'Папапапа', '<p>Папапапа</p>', 1, 1, NULL, NULL, '2017-01-04 05:57:43', '2017-01-04 05:57:43'),
(12, 'Вебинар на тему «3D моделирование в Украине и мире»', '1', '<p>1</p>', 1, 1, NULL, NULL, '2017-01-04 05:59:54', '2017-01-04 05:59:54'),
(13, 'Вебинар на тему «3D моделирование в Украине и мире»', 'еууе', '<p>еууе</p>', 1, 1, NULL, NULL, '2017-01-04 06:00:57', '2017-01-04 06:00:57'),
(14, 'Вебинар на тему «3D моделирование в Украине и мире»', 'рпр', '<p>рпр</p>', 1, 1, NULL, NULL, '2017-01-04 06:02:42', '2017-01-04 06:02:42'),
(15, 'Вебинар на тему «3D моделирование в Украине и мире»', 'hi', '<p>hi</p>', 2, 1, NULL, NULL, '2017-01-04 10:59:13', '2017-01-04 10:59:13');
        ");
    }
}