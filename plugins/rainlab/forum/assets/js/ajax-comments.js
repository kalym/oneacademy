var page = 1;
var isAllLoaded = false;

window.onscroll = function (ev) {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        // you're at the bottom of the page
        page++;
        loadMoreData(page);
    }
};

function loadMoreData(page) {

    if (!isAllLoaded) {
        $.request('onLoadComments', {
            data: {
                pageNumber: page
            },
            beforeSend: function () {
                $('.comments-ajax-loader').css('display', 'block');
            },
            success: function (response) {

                $('.comments-ajax-loader').css('display', 'none');


                if (response.length == 0) {
                    isAllLoaded = true;
                }

                $('#posts').append(response.result);
            }
        });
    }
}