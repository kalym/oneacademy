var app = (function(){

   var _init = function(){
       _initMedia();
       _initLoginModal();
       _initRegisterModal();
       _listenSubscribeButton();
       _initOpenTab();
       _textareaResize();
       _initProfileUserDescription();
       _initProfileChat();
       _initSocial();
       _initWeek();
   };

    var _textareaResize = function () {
        $.each($('textarea[data-autoresize]'), function () {
            var offset = this.offsetHeight - this.clientHeight;
            var resizeTextarea = function (el) {
                $(el).css('height', 'auto').css('height', el.scrollHeight + offset);
            };
            $(this).on('keyup input', function () {
                resizeTextarea(this);
            }).removeAttr('data-autoresize');
        });
    };


   var _listenSubscribeButton = function() {
        $('.open_subscribe').on('click', function(){
            $('.open_subscribe').toggleClass('open');
            $('.subscribe_block').toggleClass('open');
        })
    };

   var _initMedia = function(){
     $('.open-menu').on('click', function () {
       $('nav').toggleClass('open');
     });
     $('.close-menu').on('click', function () {
       $('nav').removeClass('open');
     });
     $('.open_dialog').on('click', function () {
         $('.chat-feed').jScrollPane().data('jsp');
       $('.chat-feed').toggleClass('open');
     });
    $(window).on('resize',function () {
        $('.chat-scroll-container').jScrollPane().data('jsp');
    });
   $('.open_list').on('click',function () {
       $('.aside-nav').toggleClass('open');
       $('.open_list_icon').toggleClass('rotate');
   });
   };

   var _initSocial = function () {
       $('.social-buttons a').on('click',function (e) {
           if($(window).width() >720){
           e.preventDefault();
           var width,height,top,left;
           width = $(window).width() > 700 ? 700 : '100%';
           top = $(window).height() > 560 ? 300 : 100;
           left =  $(window).width() / 2 - (width / 2);

           window.open($(this).attr('href'),'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=360,width='+width+',left='+left+',top='+top);
           }
       })
   };

   var _initOpenTab = function openTab() {
       $(document).on('click','.tablinks',function () {
           var tabName = $(this).data('tab-index');
           var i, tabcontent, tablinks;

           $(document).find(".tabcontent").hide();
           $(document).find(".tablinks.active").removeClass('active');
           $(document).find('#'+tabName).css("display","block");
           $(this).addClass("active");

           $('.chat-feed').jScrollPane().data('jsp');
          var chatScroll = $('.chat-scroll-container').jScrollPane().data('jsp')
              if(chatScroll)
                  chatScroll.scrollToBottom();

           if(tabName=='10'){
               $.request('onTabOpen');
           }

       });

       if(location.hash == '#new-messages'){
           $(".tablinks[data-tab-index='10']").trigger('click');
       }

       $(".show-msg").on("click",function () {
           $(".tablinks[data-tab-index='10']").trigger('click');
       })

   };

   var _sendBlogComment = function(){
    $("#topicContent").val('');
    $("html, body").animate({ scrollTop: $(document).height() }, "slow");
    return false;
   };

   var _initWeek = function () {


       $(document).on('submit','#week-send-comment-form', function (e) {
           e.preventDefault();

           if ( $('#weekCommentContent').val()) {
               var submitBtn = $(this).find(':submit');
               submitBtn.attr('disabled', true);

               $.request('onSendComment', {
                   data: $(this).serializeArray(),
                   success: function (response) {
                       submitBtn.attr('disabled', false);
                       $('#weekCommentContent').val('');
                       $('#week-comments').html(response.result);
                   }
               });
           }
       });

       $('.course-week').on('click', function () {

       $('.active-week').removeClass('active-week');
       $(this).addClass('active-week');

           var container = $('#week-content');

           container.html(' ' +
               '<div class="comments-ajax-loader" style="display: block;margin: 200px auto;text-align: center;position: relative;z-index:50;"> '+
                '<img src="/plugins/rainlab/forum/assets/images/ajax-loader.gif">'+
               '</div>'
           );

           $.request('onLoadWeek', {
                data: {
                    'week_id' : $(this).attr('data-week-id')
                },
                success: function (response) {
                    var tabs = $('.tablinks');

                    tabs.removeClass('active');
                    tabs.first().addClass('active');
                    container.html(response.content);
                    $('#week-title').html(response.title);
                    $('.responsiv-uploader-fileupload').fileUploader();
                }
           });
       });
   };

   var _initProfileChat = function () {
       $(document).on('click', '.selected-dialog', function (e) {
           e.preventDefault();
           formDisabling = true;
           $('.comments-ajax-loader').show();

           if(!$(this).hasClass('open-dialog')){

               var dialogId =  $(this).attr('data-dialog-id');

               $('.feed-box .open-dialog').removeClass('open-dialog');
               $('.chat-feed.open').removeClass('open');
               $(this).addClass('open-dialog');

               $('#dialog-messages-container .jspPane').html('');

               $.request('onLoadMessages', {
                   data: {
                       dialog_id : dialogId
                   },
                   success: function(response) {
                       if(response.result != undefined){
                        $('#dialog-messages-container .jspPane').html(response.result);
                           $('.chat-scroll-container').jScrollPane().data('jsp').scrollToBottom();
                       }
                       $('#chat-current-dialog-id').val(dialogId);
                       $('.comments-ajax-loader').hide();
                       formDisabling = false;
                       $('#messageContent').focus();
                   }
               });
           }
       });

       $('#messageContent').on('keydown',function (e) {
           if ( e.which == 13 ) {
               $('#chat-send-message-form').trigger('submit');
           }
       });

       $('#chat-send-message-form').on('submit', function (e) {
           e.preventDefault();
           var content = $('#messageContent');

           if (content.val().replace(/^\s+|\s+$/g,"") != '' && formDisabling != true){
               var formData = $(this).serializeArray();
               var date = new Date();
               var time = date.getMilliseconds();
               var templateMsg =
                   '<div class="chat-message-coming chat-message-wait t' + time + '">' +
                   '<p>' + content.val() + '</p>' +
                   ' </div>';

               content.val('');

               $('#dialog-messages-container .jspPane').append(templateMsg);

               var scrollPane = $('.chat-scroll-container').jScrollPane().data('jsp');
               scrollPane.scrollToBottom();


               $.request('onSendMessage', {
                   data: formData,
                   success: function (response) {
                       if (response) {
                           $(document).find('.t' + time).append('<span>' + response.result + '</span>').removeClass('chat-message-wait', 't' + time)
                       }
                   }
               })
           }else{
               content.val(content.val().replace(/^\s+|\s+$/g,""));
           }
       });
   };

   var formDisabling = false;

   var _initProfileUserDescription = function () {

       $(".cabinet-edit-btn").on('click', function (e) {
           e.preventDefault();

          var saveSpan = $('#profile-description-save');
           var editSapn = $('#profile-description-edit');
           var container = $('.cabinet-edit-description');
           var button  = $(this);

           if(!button.hasClass('edittable')){
               container.attr('contenteditable','true');
               container.focus();
               container.addClass('cabinet-edit-description-active');
               saveSpan.css('display', 'block');
               editSapn.css('display', 'none');
               button.addClass('edittable');
           }else{
               $.request('onUpdateUserDescription', {
                   data: {
                       description: container.text()
                   },
                   success:{}
               });
               container.attr('contenteditable','false');
               container.removeClass('cabinet-edit-description-active');
               saveSpan.css('display', 'none');
               editSapn.css('display', 'block');
               button.removeClass('edittable');
           }

       });

       $(document).on('keydown','.cabinet-edit-description-active',function (e) {
           if ( e.which == 13 ) {
               $('.cabinet-edit-btn').trigger('click');
           }
       });


   };

   var _initRegisterModal = function () {
       var modal = $("#registerModal");
       var loginModal = $("#loginModal");

       var btn = $("#openRegisterModal");
       var span = $(".close");

       btn.on("click", function(e) {
           e.preventDefault();

           $('.modal').hide();
           modal.show();
           $('html').css('height','100%').css('overflow','hidden');

       });

       span.on("click", function() {
           modal.hide();
           $('html').css('overflow','auto');

       });

       $(window).on("click", function(event) {
           if (event.target == modal) {
               modal.hide();
               $('html').css('overflow','auto');
           }
       });
   };

    var _initLoginModal = function () {
    var modal = $("#loginModal");
    var btn = $("#openModal");
    var span = $(".close");
    btn.on("click", function() {
        $('.modal').hide();
        modal.show();
        $('html').css('height','100%').css('overflow','hidden');

    });
    span.on("click", function() {
        modal.hide();
        $('html').css('overflow','auto');
    });
    $(window).on("click", function(event) {
        if (event.target == modal) {
            modal.hide();
            $('html').css('overflow','auto');
        }
    });
   };

    return{
      init : _init,
      sendBlogComment: _sendBlogComment
    }
})();
(function () {
    $('.chat-scroll-container').jScrollPane();
})();


$(document).ready(function(){
    app.init();
});

