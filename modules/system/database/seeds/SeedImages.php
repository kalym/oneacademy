<?php namespace System\Database\Seeds;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Seeder;
use System\Models\File;

/**
 * Created by PhpStorm.
 * User: Maxim.Drobonoh
 * Date: 11.01.2017
 * Time: 18:06
 */

class SeedImages extends Seeder
{

    public function run()
    {
        DB::statement("
        INSERT INTO `system_files` (`id`, `disk_name`, `file_name`, `file_size`, `content_type`, `title`, `description`, `field`, `attachment_id`, `attachment_type`, `is_public`, `sort_order`, `created_at`, `updated_at`) VALUES
(2, '582c2a980a1ac562064846.png', 'sprite.png', 26149, 'image/png', NULL, NULL, 'logo', '1', 'Backend\\\Models\\\BrandSetting', 1, 2, '2016-11-16 06:44:56', '2016-11-16 06:44:59'),
(13, '5832c7792df35901052644.jpg', 'feed-list2.jpg', 27607, 'image/jpeg', NULL, NULL, 'feed_poster', '1', 'Untitled\\\Feeds\\\Models\\\Feed', 1, 13, '2016-11-21 07:07:53', '2016-11-21 07:07:55'),
(18, '583827aedc6e6540556806.jpg', 'courses-item1.jpg', 29082, 'image/jpeg', NULL, NULL, 'curs_poster', '20', 'Untitled\\\Curses\\\Models\\\Curs', 1, 18, '2016-11-25 08:59:42', '2016-11-25 08:59:44'),
(19, '583827b7bba06479238051.jpg', 'courses-item2.jpg', 23486, 'image/jpeg', NULL, NULL, 'curs_poster', '21', 'Untitled\\\Curses\\\Models\\\Curs', 1, 19, '2016-11-25 08:59:51', '2016-11-25 08:59:52'),
(20, '583827c1c99f0002818274.jpg', 'courses-item3.jpg', 24855, 'image/jpeg', NULL, NULL, 'curs_poster', '22', 'Untitled\\\Curses\\\Models\\\Curs', 1, 20, '2016-11-25 09:00:01', '2016-11-25 09:00:03'),
(21, '58415ff5ca699538352113.jpg', 'strem1.jpg', 29082, 'image/jpeg', NULL, NULL, 'avatar', '2', 'RainLab\\\User\\\Models\\\User', 1, 21, '2016-12-02 08:50:13', '2016-12-02 08:50:15'),
(22, '58614ffcdbe44167748003.jpg', 'feed-list2.jpg', 27607, 'image/jpeg', NULL, NULL, 'content_images', '1', 'RainLab\\\Blog\\\Models\\\Post', 1, 22, '2016-12-26 14:14:36', '2016-12-26 14:14:42'),
(23, '58615140a78bd367821744.jpg', 'feed-list1.jpg', 27550, 'image/jpeg', NULL, NULL, 'featured_images', '1', 'RainLab\\\Blog\\\Models\\\Post', 1, 1, '2016-12-26 14:20:00', '2016-12-26 14:29:13'),
(24, '58626be1bcb2d111014119.jpg', 'article1.jpg', 29378, 'image/jpeg', NULL, NULL, 'content_images', '1', 'RainLab\\\Blog\\\Models\\\Post', 1, 24, '2016-12-27 10:25:53', '2016-12-27 10:26:23'),
(25, '58628100bbe85882201410.jpg', 'feed-vlist3.jpg', 24154, 'image/jpeg', NULL, NULL, 'content_images', '2', 'RainLab\\\Blog\\\Models\\\Post', 1, 25, '2016-12-27 11:56:00', '2016-12-27 11:56:32'),
(26, '5862819960891788258583.jpg', 'courses-item1.jpg', 29082, 'image/jpeg', NULL, NULL, 'featured_images', '2', 'RainLab\\\Blog\\\Models\\\Post', 1, 26, '2016-12-27 11:58:33', '2016-12-27 11:58:39'),
(29, '5862834d0990d190993412.jpg', 'article.jpg', 113533, 'image/jpeg', NULL, NULL, 'featured_images', '3', 'RainLab\\\Blog\\\Models\\\Post', 1, 29, '2016-12-27 12:05:49', '2016-12-27 12:05:52');
        ");
    }
}